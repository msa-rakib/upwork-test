<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dd', function () {
    $model = request('model', 'App\Models\User');
    return dd($model::all());
});

Route::get('/home', [\App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/artisan', function () {
    $command = request('command', 'list');
    \Artisan::call($command);
    dd(\Artisan::output());
});

Route::get('/log', function () {
    $log = file_get_contents(storage_path('logs/laravel.log'));
    dd($log);
});
<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::with('author')
        ->when($request->get('s'), function($query) use($request){
            $query->whereHas('author', function($query) use($request){
                $query->where('name', 'like', '%'.$request->get('s').'%');
            })
            ->orWhere('title', 'like', '%'.$request->get('s').'%')
            ->orWhere('created_at', 'like', '%'.$request->get('s').'%');
        })->paginate(20);

        return view('home', compact('products'));
    }
}

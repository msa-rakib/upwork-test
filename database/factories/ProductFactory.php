<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            // 'id' => random_int(1, 100),
            'title' => $this->faker->sentence(3),
            'slug' => $this->faker->unique()->slug,
            'description' => $this->faker->paragraph(3),
            'price' => $this->faker->randomFloat(2, 10, 100),
            'user_id' => \App\Models\User::inRandomOrder()->first()->id,
        ];
    }
}
